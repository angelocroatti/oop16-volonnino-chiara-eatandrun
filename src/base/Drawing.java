package base;

/**
 * Interface implemented by all graphic objects presents into the game
 */

import java.awt.Graphics;

/**
 * 
 * @author Chiara
 *
 */

public interface Drawing {
    
    /**
     * 
     * @param graphic
     *          graphic's a specific element 
     */
    public void draw(Graphics graphic);

}
